# rpiezos

This is a system composed by a shield to mount on the raspberry pi and a server application for translating piezo sensors signals into OSC messages.

<p align="center">
  <img src="http://nonmateria.com/data/repos/rpiezos/prototype.jpg" width="700">
</p>

## Usage
```
rpiezos [options]
```
    
option list:

```
-i [destination IP] : sets the destination IP address, localhost if not given
-p [destination port] : sets the destination port. This is mandatory to set.
-a [destination address] : sets the destination OSC address. defaults to /piezo
-s [piezos options] : sets the active piezos and range, see below     
-u [usec sleep] : sets the delay in the main loop in usec, defaults to 500     
-v [verbose] : activate a verbose output, doesn't need any argument      
```

after the `-p` option you can give a list of number from 0 to 5, to activate the correlated sensors, optionally you can add some `:` to set ranges, value outside the ranges won't be sent. For example:

```
rpiezos -p 12345 -s 012345
```

sends all the sensors values to localhost:12345

```
rpiezos -i 192.168.0.44 -p 3333 -s 01:10:512
```

sends sensors 0 and 1 values to 192.168.0.44:3333, only if the values are from 10 to 512

```
rpiezos -a /test -p 3333 -s 2:10:1024 -s 3:5:1024
```

sends messages to localhost:3333, at the OSC address /test, sends just the values from 10 to 1024 of sensor 2 and from 5 to 1024 of sensor 3.

The messages will be sent to the set address (default to `/piezo`) and they will have two arguments: an int for the sensor number and a normalized float for the signal value (low-high 10 bit range is remapped to 0.0f <--> 1.0f ).

## Building 

First, remember to enable SPI with `sudo raspi-config` or by putting `dtparam=spi=on` into your `/boot/config.txt`.

Then you need `liblo` installed (included dev packages) in your system, then you can build with make. 
On raspian buster for example:

```sh
sudo apt-get install build-essential
sudo apt-get install liblo7 liblo-dev
git clone https://git.sr.ht/~npisanti/rpiezos
cd rpiezos 
make

```

then you will have an executable in `bin/rpiezos`. 

## Hardware
For more info on how to build the hardware, go to the [hardware](https://git.sr.ht/~npisanti/rpiezos/tree/master/hardware/README.md) readme.

## Thanks to 
Dario Longobardi's  [ofxGPIO](https://github.com/kashimAstro/ofxGPIO), most of the code is ported from the `mcp.h` file.

## License 
Nicola Pisanti MIT License 2020.
